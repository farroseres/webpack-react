const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const path = require('path');
require('dotenv').config();

const devMode = process.env.NODE_ENV !== 'production';

module.exports = {
  watch: devMode,

  entry: "./src/index.js",

  output: {
    path: path.resolve('./dist'), // Webpack à besoin d'un chemin absolue, on se sert de path.resolve pour le générer 
    filename: "bundle.js",
  },

  module: {
    rules: [
      {
        // Regex permettant de tester les fichiers à prendre en compte
        test: /\.js$/,

        // Important ! node_modules doit être exclu des fichiers à tester
        exclude: /node_modules/,

        // Permet de définir les loaders à utiliser
        use: [
          "babel-loader",
        ]
      },
      {
        test: /\.s?css$/,
        use: [
          devMode ? { loader: "style-loader", options: { injectType: "styleTag" } } : MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.(png|jpg|svg)$/,
        type: 'asset',
        generator: {
          filename: 'images/[name][ext]'
        }
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        type: 'asset',
      },
    ]
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/[name].css',
    }),
    new HtmlWebpackPlugin( { template: './public/index.html' } ),
  ],

  devServer: {
    hot: true,
    client: {
      overlay: true,
    },
  },

  resolve: {
    alias: {
      "@": path.resolve('./src/'), 
    },
  }
}