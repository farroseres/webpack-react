import React, { useState } from "react";

export default () => {
  const [count, setCount] = useState(0);

  const handleIncrement = () => {
    setCount(count+1);
  }

  const handleDecrement = () => {
    setCount(count-1);
  }

  return (
    <div className="counter">
      <button onClick={handleDecrement}>-</button>
      <input type="text" value={count} />
      <button onClick={handleIncrement}>+</button>
    </div>
  )
}